The code should run fine on its own, as long as you have the two CSV files in the directory.
Without these files, the code cannot run as pre-processing has been performed outside of R.
Confusion matrices can be viewed by simply typing the name of a table in R, e.g. 'tSVM4'.